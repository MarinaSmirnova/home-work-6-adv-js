const btn = document.getElementById("btn");
const container = document.getElementById("container");


async function getData(url) {
    try {
        const response =  await fetch(url);

        if (!response.ok) {
            throw new Error("Помилка при запиті");
          }

        const data = await response.json();

        return data;

    } catch (e) {
        console.log("Error: " + e);
    }
}


async function showIpInfo() {
    const {ip} = await getData("https://api.ipify.org/?format=json");
    const {country, regionName, city, countryCode, timezone, zip} = await getData(`http://ip-api.com/json/${ip}`);

    container.innerHTML = `
    <p>IP: ${ip}</p>
    <p>Country: ${country}</p>
    <p>Code: ${countryCode}</p>
    <p>Region: ${regionName}</p>
    <p>City: ${city}</p>
    <p>Zip: ${zip}</p>
    <p>Timezone: ${timezone}</p>
    `;
}

btn.addEventListener("click", () => {showIpInfo()});